#!/usr/bin/perl
#
# For the passed model, will make sure the following files exist, and create
# them if necessary:
#   * Preview picture
#   * .civitai.json (a lot of info taken from civitai)
#   * .info (the full civitai description)
#   * .description (trigger words, example pic)
#

use strict;
use warnings;
use HTTP::Tiny;
use JSON;
use Digest;
use Getopt::Long;
use HTML::FormatText::WithLinks;
use Text::Wrap;
use utf8;
no warnings 'utf8';


my $api_url = 'https://civitai.com/api/v1';
my $cache_dir = '/tmp/cache';

# Options
my $update;         # re-create files, even if they already exist
my $onlypreview;    # only update if preview pic is missing
my $versionid;      # allow passing a specific ID (not implemented yet)
my $forcehash;      # override the hash
my $debug;          # enable debug output

Getopt::Long::Configure ("bundling");
GetOptions (
    'update|u'   => \$update,
    'version|i=i'=> \$versionid,
    'hash|a=s'   => \$forcehash,
    'preview|p'  => \$onlypreview,
    'debug|d'    => \$debug,
) || die "Usage: $0 [--update|-u] [--debug|d]\n";


my $json_codec = JSON->new->allow_nonref->pretty;
my $ua = HTTP::Tiny->new();
my $html_formatter = HTML::FormatText::WithLinks->new(
    before_link => '',
    after_link => ' [%l]',
    footnote => '',
    with_emphasis => 1,
    skip_linked_urls => 1,
);

$api_url .= '/' unless $api_url =~ m!/$!;
mkdir $cache_dir unless -d $cache_dir;

my $multi_files;
$multi_files = 1 if @ARGV > 1;

while (my $fn = shift @ARGV) {

    if ( ! -e $fn ) {
        printf STDERR "%s does not exist, skipping.\n", $fn;
        next;
    }
    my $fn_size = (stat(_))[7];

    # Don't work on VAEs
    if ($fn =~ /\.vae\.pt$/ or $fn =~ m!(^|/)VAE!) {
        printf STDERR "Ignoring VAE %s\n", $fn unless $multi_files;
        next;
    }
    # Ignore the files that this script itself is generating
    # as well as previews generated via the UI, YAMLs,...
    if ($fn =~ /\.(png|jpg|jpeg|webp|json|yaml|info|txt)$/) {
        printf STDERR "Ignoring %s\n", $fn unless $multi_files;
        next;
    }

    # filenames
    (my $basename = $fn) =~ s/\.[^.]*$//;   # strip extension
    my $json_fn = "$basename.civitai.json";
    my $info_fn = "$basename.info";
    my $desc_fn = "$basename.description.txt";
    my $skip_fn = "$basename.noinfo";

    if (-e $skip_fn) {
        printf STDERR "DBG: Skipping '%s', to be ignored.\n", $fn if $debug or not $multi_files;
        next;
    }

    # Preview pictures can have various names, let's search for all of them
    my $prev_exists;
    for my $base ($basename, "$basename.preview") {
        for my $ext ('png', 'jpg', 'webp') {
            $prev_exists = 1 if -e "$base.$ext";
        }
    }

    # If all files already exist, and "-u" was not given, we can just skip
    # this model completely
    if ($prev_exists and -e $json_fn and -e $info_fn and -e $desc_fn
            and not $update) {
        printf STDERR "DBG: Skipping '%s', already done.\n", $fn if $debug or not $multi_files;
        next;
    }

    # When called with -p, only update files which have no preview
    if ($onlypreview and $prev_exists) {
        printf STDERR "DBG: Skipping '%s', preview exists.\n", $fn if $debug or not $multi_files;
        next;
    }

    printf STDERR "Working on '%s'\n", $fn;

    # Retrieve info from civitai (if necessary)
    my $json;
    if (! -e $json_fn or $update) {
        # [XXX] if $versionid is set, retrieve that (and the model info)

        # Calculate hash (SHA256 digest)
        # unless user has already specified a versionid/forcehash
        my $hash;
        unless ($versionid or $forcehash) {
            my $ctx = Digest->new("SHA-256");
            open my $fh, '<', $fn or die "$!.";
            $ctx->addfile($fh);
            close $fh;
            $hash = $ctx->hexdigest;
            printf STDERR "    DBG: hash = %s\n", $hash if $debug;
        } elsif ($forcehash) {
            $hash = $forcehash;
            printf STDERR "    Looking for hash %s\n", $hash;
        }

        if ($hash) {
            $$json{version} = api("model-versions/by-hash/$hash");
        } elsif ($versionid) {
            $$json{version} = api("model-versions/$versionid");
        } else {
            die;
        }

        unless ($$json{version}) {
            printf STDERR "    Can't find model-version %s on civitai.\n",
                $hash ? uc substr($hash, 0, 10) : $versionid;
            next;
        }

        # Retrieve the model itself -- only needed for the description
        $$json{model} = api("models/$$json{version}{modelId}");
        unless ($$json{model}) {
            printf STDERR "    Can't find model %d on civitai.\n",
                $$json{version}{modelId};
            next;
        }

        # Write retrieved information to file
        open my $fh, '>', $json_fn or die "Can't write $json_fn: $!.";
        print $fh $json_codec->encode($json);
        close $fh;
        printf STDERR "    DBG: Created %s\n", $json_fn if $debug;

    } else {
        # Read existing json
        local $/ = undef;
        open my $fh, '<', $json_fn or die "Can't read $json_fn: $!.";
        $json = $json_codec->decode(<$fh>);
        close $fh;
    }

    printf STDERR "    %s: %s - %s\n",
        $$json{version}{model}{type}, $$json{version}{model}{name}, $$json{version}{name};

    # Write description file
    if (! -e $desc_fn or $update) {
        open my $fh, '>', $desc_fn or die "Can't write $desc_fn: $!.";

        # This will be show as HTML when hovering over the thumbnail!
        # If the text is too big to fit, only the bottom will be shown
        # -> we move the most important info (trigger words) to the bottom

        # Name - Version
        printf $fh "<b>%s - %s</b><br>\n",
            $$json{version}{model}{name}, $$json{version}{name};

        # Version description (might be empty/undef)
        if (exists $$json{version}{description} and
            $$json{version}{description}) {
            print $fh "\n";
            print $fh $$json{version}{description};
        }

        # Show the baseModel, in case it's not the "normal" 1.5
        if (exists $$json{version}{baseModel} and
            $$json{version}{baseModel} ne 'SD 1.5') {
            printf $fh "\n<b>%s</b><br>\n", $$json{version}{baseModel};
        }

        # Trigger words (not all models have/need them)
        if (exists $$json{version}{trainedWords} and
            ref $$json{version}{trainedWords} eq 'ARRAY') {
            printf $fh "Trigger: <b>%s</b><br>\n",
                join ',', @{$$json{version}{trainedWords}};
        }

        close $fh;
        printf STDERR "    DBG: Created %s\n", $desc_fn if $debug;
    }


    # Write info file
    if (! -e $info_fn or $update) {
        open my $fh, '>', $info_fn or die "Can't write $info_fn: $!.";

        # Shown as pre-formatted text when clicking the little [i] icon
        # We need to convert all HTML (descriptions) to plain text

        # Name - Version
        printf $fh "%s - %s\n",
            $$json{version}{model}{name}, $$json{version}{name};

        # Trigger words (not all models have/need them)
        if (exists $$json{version}{trainedWords} and
            ref $$json{version}{trainedWords} eq 'ARRAY') {
            printf $fh "\nTrigger: %s\n",
                wrap('', "\t", join ',', @{$$json{version}{trainedWords}});
        }

        # Example prompts (max 3)
        if (exists $$json{version}{images}) {
            my $prompt = 1;
            for my $img (@{$$json{version}{images}}) {
                if ($$img{meta}{prompt}) {
                    $Text::Wrap::columns = 120;
                    printf $fh "\n┍━━━ Example %d ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\n", $prompt;
                    $$img{meta}{prompt} =~ s/\s+/ /g;
                    printf $fh "│   Prompt: %s\n", wrap('', ' 'x12, $$img{meta}{prompt});
                    $$img{meta}{negativePrompt} =~ s/\s+/ /g if $$img{meta}{negativePrompt};
                    printf $fh "│ Negative: %s\n", wrap('', ' 'x12, $$img{meta}{negativePrompt}) if $$img{meta}{negativePrompt};

                    my @details;
                    push @details, "CFG: $$img{meta}{cfgScale}" if $$img{meta}{cfgScale};
                    push @details, "Seed: $$img{meta}{seed}" if $$img{meta}{seed};
                    push @details, "$$img{meta}{sampler}/$$img{meta}{steps}" if $$img{meta}{sampler} and $$img{meta}{steps};
                    push @details, "$$img{meta}{Size}" if $$img{meta}{Size};
                    push @details, "$$img{meta}{Model}" if $$img{meta}{Model};
                    push @details, "CLIP: $$img{meta}{'Clip skip'}" if $$img{meta}{'Clip skip'};
                    printf $fh "│ %s\n", join '  ', @details;
                    last if $prompt++ == 3;
                }
            }
            print $fh "\n" if $prompt > 1;
        }

        # Version description (might be empty/undef)
        if (exists $$json{version}{description} and
            $$json{version}{description}) {

            print $fh "\n┍━━━ Description of this version ━━━━━━━━━━━━━━━━━━━━━━━━\n";

            my $txt = $html_formatter->parse($$json{version}{description});
            $txt =~ s/^   //mg; # strip left margin
            $txt =~ s/^\s+//;   # strip initial whitespace (incl. newlines)
            $txt =~ s/^/│ /mg;   # prefix each line
            print $fh $txt;

        }

        # Model description (might be empty/undef)
        if (exists $$json{model}{description} and
            $$json{model}{description}) {

            print $fh "\n┍━━━ Description of model ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\n";

            my $txt = $html_formatter->parse($$json{model}{description});
            $txt =~ s/^   //mg; # strip left margin
            $txt =~ s/^\s+//;   # strip initial whitespace (incl. newlines)
            $txt =~ s/^/│ /mg;  # prefix each line
            print $fh $txt;
        }

        close $fh;
        printf STDERR "    DBG: Created %s\n", $info_fn if $debug;
    }

    # Preview image
    if (! $prev_exists or $update) {
        if (exists $$json{version}{images} and
            @{$$json{version}{images}} and      # at least 1 image
            exists $$json{version}{images}[0]{url}) {

            my $url = $$json{version}{images}[0]{url};
            printf STDERR "    Getting preview image\n";
            printf STDERR "        DBG: %s\n", $url if $debug;
            my $response = $ua->get($url);

            if ($response->{success}) {
                my $content = $response->{content};

                # figure out extension
                my $ext;
                # try response header
                my $ct = ${$response->{headers}}{'content-type'};
                if (defined $ct and $ct) {
                    printf STDERR "        DBG: Content-Type %s\n", $ct if $debug;
                    if ($ct eq 'image/jpeg') {
                        $ext = 'jpg';
                    } elsif ($ct eq 'image/png') {
                        $ext = 'png';
                    }
                    printf STDERR "        DBG: -> %s\n", $ext if $debug;
                }
                # otherwise, just use the URL
                if (! defined $ext and $url =~ /\.(\w{3,5})$/) {
                    $ext = $1;
                    printf STDERR "        DBG: URL says %s\n", $ext if $debug;
                }
                # ok, that's bad, let's hope that "png" works, I guess?
                $ext = 'png' unless defined $ext;

                my $prev_fn = "$basename.preview.$ext";
                open my $fh, '>', $prev_fn or die "Can't write to $prev_fn: $!.";
                print $fh $content;
                close $fh;
                printf STDERR "    DBG: Created %s\n", $prev_fn if $debug;
            } else {
                printf STDERR "        GET %s -> %d (%s)\n", $url, $response->{status}, $response->{reason};
            }

        } else {
            printf STDERR "    DBG: no preview image found.\n";
        }
    }

    # Make sure that we don't update multiple files when a versionid was
    # specified
    last if $versionid or $forcehash;
}


sub api {
    my ($url) = @_;

    printf STDERR "    DBG: API-call %s\n", $url if $debug;

    # Prepare cache filename
    (my $noslash = $url) =~ s!/!_!g;    # replace slashes by underscores
    my $cache_fn = "$cache_dir/$noslash.json";

    my $content;

    if (-e $cache_fn) {
        printf STDERR "        DBG: using cache %s\n", $cache_fn if $debug;
        local $/ = undef;
        open my $fh, '<', $cache_fn or die "$cache_fn: $!.";
        $content = <$fh>;
        close $fh;

    } else {
        my $response = $ua->get($api_url . $url);
        # [XXX] rate limiting
        sleep 0.2;
        unless ($response->{success}) {
            printf STDERR "        DBG: GET %s -> %d (%s)\n", $url, $response->{status}, $response->{reason} if $debug;
            return;
        }
        $content = $response->{content};

        # store into cache file
        open my $fh, '>', $cache_fn or die "Can't write to $cache_fn: $!.";
        print $fh $content;
        close $fh;
    }

    return $json_codec->decode($content);
}

