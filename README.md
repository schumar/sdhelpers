# Create auxiliary files from civitai data

This script will create the following files (if they don't exist) for a given
model, to be used in e.g. [Vladimirs's SD.next](https://github.com/vladmandic/automatic)

* `.preview.[extension]`, preview image
* `.civitai.json`, all information about the model from civitai
* `.description.txt`, short information (shown when hovering over the thumbnail)
* `.info`, full description of the model and example prompts (shown when clicking the `i`nformation icon)

## Prerequisites

You need Perl (all Debian-based distributions should have this, others can
surely install it easily), as well as some additional Perl modules:

* `HTML::FormatText::WithLinks`
* `HTTP::Tiny`
* `JSON`

On Debian, you can get those with
```bash
apt install libhttp-tiny-perl libhtml-formattext-withlinks-perl libjson-perl
```

## Usage

```
civitai_to_info.pl [-u] [-d] [-a hash] [-i versionid] model

e.g.
civitai_to_info.pl -d ~/vladmandic-automatic/models/Stable-diffusion/aZovyaPhotoreal_v2.safetensors
or
civitai_to_info.pl ~/vladmandic-automatic/models/Stable-diffusion/*

or, if you want to get fancy and run this on ALL your models:

find ~/vladmandic-automatic/models -type f \
    \( -name '*.safetensors' -o -name '*.ckpt' -o -name '*.pt' -o -name '*.bin' \) -print0 \
    | sort -z | xargs -0 nice civitai_to_info.pl
```

`-u` is "force-update" (recreate files even if they exist already)

`-p` will ignore all files for which preview-images already exist

`-d` is "debug" (will generate some debug output. Nice to look at ;)

`-a`/`-i` allow you to override the hash/versionid, to pick a specific version from civitai. (sometimes their hash-calculations are wrong)

## Links

* [Vladimirs's SD.next](https://github.com/vladmandic/automatic)
* [Discussion which lead to me writing this script](https://github.com/vladmandic/automatic/discussions/1629)

## About this Project

I needed to scratch an itch. So this is not a beacon of good programming, and yes, it's in Perl,
because that's the perfect tool for this job.

Feel free to open an [issue](https://gitlab.com/schumar/sdhelpers/-/issues) if you need help,
found a bug, have a good idea for improvement,...
